import dessertReducer from "./dessertReducer";

import { combineReducers } from "redux";

const rootReducer = combineReducers({
  dessertReducer,
});

export default rootReducer;
