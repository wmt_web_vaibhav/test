const initialData = { list: [] };

const dessertReducer = (state = initialData, action) => {
  switch (action.type) {
    case "ADD_DESSERT":
      const { id, data } = action.payload;

      return {
        ...state,
        list: [
          ...state.list,
          {
            id,
            ...data,
          },
        ],
      };

    case "EDIT_DESSERT":
      const { id: editid, data: editdata } = action.payload;

      const editlist = state.list.map((elem, index) => {
        if (elem.id == editid) {
          return {
            id: editid,
            name: editdata.name,
            calories: editdata.calories,
            fat: editdata.fat,
            carbs: editdata.carbs,
            protein: editdata.protein,
          };
        } else {
          return elem;
        }
      });

      return {
        ...state,
        list: editlist,
      };

    case "DELETE_DESSERT":
      const newlist = state.list.filter((elem) => elem.id !== action.id);

      return {
        ...state,
        list: newlist,
      };

    default:
      return state;
  }
};

export default dessertReducer;
