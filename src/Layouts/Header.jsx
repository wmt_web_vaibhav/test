import React from 'react';
import './header.scss';
import {
  AppBar,
  Button,
  IconButton,
  Toolbar,
  Typography,
} from '@material-ui/core';
import {
  makeStyles,
  Theme,
  createStyles,
} from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from 'react-router-dom';
import Login from '../Login/Login';
import LogoutIcon from '@material-ui/icons/ExitToApp';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

function Header() {
  const classes = useStyles();
  const token = localStorage.getItem('authtoken');

  const logout = () => {
    localStorage.removeItem('authtoken');
    localStorage.removeItem('user');
  };

  return (
    <AppBar position="static" className="headercss">
      <Toolbar>
        <Typography variant="h6" className={classes.title}>
          Simple-App
        </Typography>
        {!token ? (
          <Link to="/" style={{ textDecoration: 'none' }}>
            <Button style={{ color: 'white' }}>Login</Button>
          </Link>
        ) : (
          <Link to="/" style={{ textDecoration: 'none' }}>
            <Button style={{ color: 'white' }} onClick={logout}>
              <LogoutIcon />
              &nbsp; Logout
            </Button>
          </Link>
        )}
      </Toolbar>
    </AppBar>
  );
}

export default Header;
