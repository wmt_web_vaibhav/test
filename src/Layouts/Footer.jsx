import React from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Grid from "@material-ui/core/Grid";
// import "./footer.css";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },

    responsivefooter: {
      [theme.breakpoints.down("xs")]: {
        padding: "0",
      },
      [theme.breakpoints.up("md")]: {
        padding: "0px 50px",
      },
      padding: "0px 15px",
      color: "#9A9A9A",
    },

    content1: {
      [theme.breakpoints.down("sm")]: {
        width: "100%",
        order: 2,
        marginTop: "10px",
        padding: "0px 15px",
        textAlign: "center",
      },
    },

    content2: {
      [theme.breakpoints.down("sm")]: {
        width: "100%",
        order: 1,
        marginTop: "5px",
        display: "flex",
        justifyContent: "center",
      },
    },

    footer: {
      background: "white",
      padding: 10,
    },

    flexContainer: {
      display: "flex",
      flexWrap: "wrap",
      flexDirection: "row",
      padding: 0,
    },

    listitem: {
      paddingTop: 0,
      paddingBottom: 0,
      flex: "0 0",
      boxSizing: "border-box",
      display: "flex",
      color: "black",
    },

    link: {
      textDecoration: "none",
      padding: 0,
      color: "black",
    },
  })
);

function Footer() {
  const classes = useStyles();
  return (
    <div className={classes.footer}>
      <Grid
        container
        justifyContent="space-between"
        className={classes.responsivefooter}
      >
        <Grid item className={classes.content1}>
          <Typography variant="subtitle2">
            Copyright © {new Date().getFullYear()} Simple App. All Right
            Reserved
          </Typography>
        </Grid>
        <Grid item className={classes.content2}>
          <List component="nav" className={classes.flexContainer} dense={true}>
            <ListItem className={classes.listitem}>
              <a className={classes.link} href="#">
                <Typography variant="subtitle2">Terms</Typography>
              </a>
            </ListItem>
            <ListItem className={classes.listitem}>
              <a className={classes.link} href="#">
                <Typography variant="subtitle2">Privacy</Typography>
              </a>
            </ListItem>
            <ListItem className={classes.listitem}>
              <a className={classes.link} href="#">
                <Typography variant="subtitle2">FAQs</Typography>
              </a>
            </ListItem>
            <ListItem className={classes.listitem}>
              <a className={classes.link} href="#">
                <Typography variant="subtitle2">Contact</Typography>
              </a>
            </ListItem>
          </List>
        </Grid>
      </Grid>
    </div>
  );
}

export default Footer;
