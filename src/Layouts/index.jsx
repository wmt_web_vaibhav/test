import React, { useEffect } from "react";
import Header from "./Header";
import Footer from "./Footer";
import "./index.scss";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import { useNavigate } from "react-router-dom";

const theme = createTheme({});

function Layout(props) {
  const navigate = useNavigate();

  return (
    <div className="main">
      <ThemeProvider theme={theme}>
        <Header className="header" />
        <div className="content">{props.children}</div>
        <Footer className="footer" />
      </ThemeProvider>
    </div>
  );
}

export default Layout;
