import {
  FormControl,
  TextField,
  useFormControl,
  Input,
  InputLabel,
  Button,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import API from "../api";
import Layout from "../Layouts/index";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(2),
    border: "2px solid grey",
    borderRadius: "15px",
    padding: "35px",
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "300px",
    },
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
    },
  },
}));

const Login = (props) => {
  const classes = useStyles();
  let navigate = useNavigate();
  const [formData, setFormData] = useState({
    username: "",
    password: "",
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (formData.username !== "" && formData.password !== "") {
      await API.post("http://localhost:8000/login", formData).then(
        ({ data }) => {
          localStorage.setItem("authtoken", data.token);
          localStorage.setItem("user", JSON.stringify(data.user));
        }
      );
      navigate("/dashboard");
    }
  };

  return (
    <Layout>
      <div
        style={{
          // height:"100%",
          justifyContent: "center",
          justifyItems: "center",
          display: "flex",
          marginTop: "100px",
        }}
      >
        <form className={classes.root} onSubmit={handleSubmit}>
          <p style={{ fontSize: "22px" }}>LOGIN</p>
          <FormControl>
            <TextField
              id="outlined-basic"
              label="username"
              variant="outlined"
              name="username"
              value={formData.username}
              onChange={(e) => {
                setFormData({
                  username: e.target.value,
                  password: formData.password,
                });
              }}
            />
          </FormControl>
          <FormControl>
            <TextField
              id="outlined-basic"
              label="password"
              variant="outlined"
              name="password"
              type="password"
              value={formData.password}
              onChange={(e) =>
                setFormData({
                  username: formData.username,
                  password: e.target.value,
                })
              }
            />
          </FormControl>
          <Button type="submit" variant="contained">
            Login
          </Button>
          <Link to="/register" style={{ textDecoration: "none" }}>
            Register now!
          </Link>
        </form>
      </div>
    </Layout>
  );
};

export default Login;
