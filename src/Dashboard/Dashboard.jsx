import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  FormControl,
  Grid,
  IconButton,
  Input,
  InputLabel,
  Paper,
  Slide,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import React, { useEffect, useState } from "react";
import Layout from "../Layouts/index";
import Edit from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { addDessert, deleteDessert, editDessert } from "../actions/index";
import { useSelector, useDispatch } from "react-redux";
import API from "../api";
import { PhotoCamera } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(2),

    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "300px",
    },
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
    },
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function Dashboard() {
  const classes = useStyles();
  let navigate = useNavigate();
  const [formData, setFormData] = useState({
    name: "",
    description: "",
    price: "",
    category: "",
    image: "",
  });

  const [search, setSearch] = useState("");

  const [rows, setRows] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [open1, setOpen1] = React.useState(0);

  React.useEffect(async () => {
    await API.get("http://localhost:8000/posts")
      .then(({ data }) => {
        setRows(data);
        setOpen(false);
        setFormData({
          name: "",
          description: "",
          price: "",
          category: "",
          image: "",
        });
      })
      .catch((e) => {
        console.log(e.response.data.message);
      });
  }, []);

  const handleClickOpen = () => {
    setFormData({
      name: "",
      description: "",
      price: "",
      category: "",
      image: "",
    });
    setOpen(true);
  };

  const handleClickOpenEdit = (index, row) => {
    setFormData({
      name: row.name,
      description: row.description,
      price: row.price,
      category: row.category,
      image: "",
    });
    setOpen1(index);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleCloseEdit = () => {
    setOpen1(0);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    let productData = new FormData() ;

    productData.append("name", formData?.name);
    productData.append("description", formData?.description);
    productData.append("category", formData?.category);
    productData.append("price", formData?.price);
    productData.append("image", formData?.image || "");

    await API.post("http://localhost:8000/post/create", productData)
      .then(({ data }) => {
        setOpen(false);
        setFormData({
          name: "",
          description: "",
          price: "",
          category: "",
          image: "",
        });
        setRows([...rows, ...[data]]);
        navigate("/dashboard");
      })
      .catch((e) => {
        console.log(e.response.data.message);
      });
  };

  const handleUpdate = async (e, id, index) => {
    e.preventDefault();

    let productData = new FormData();

    productData.append("name", formData?.name);
    productData.append("description", formData?.description);
    productData.append("category", formData?.category);
    productData.append("price", formData?.price);
    productData.append("image", formData?.image);

    await API.put("http://localhost:8000/post/edit/" + id, productData)
      .then(({ data }) => {
        setOpen1(0);
        setFormData({
          name: "",
          description: "",
          price: "",
          category: "",
          image: "",
        });
        let updatedrow = rows;
        updatedrow[index] = data;
        setRows(updatedrow);
        navigate("/dashboard");
      })
      .catch((e) => {
        console.log(e.response.data.message);
      });
  };

  const handleDelete = async (e, id, index) => {
    e.preventDefault();

    await API.delete("http://localhost:8000/post/delete/" + id)
      .then(({ data }) => {
        let updatedrow = rows;
        updatedrow.splice(index, 1);
        setRows(updatedrow);
        navigate("/dashboard");
      })
      .catch((e) => {
        console.log(e.response.data.message);
      });
  };

  const handleSearch = async (e) => {
    e.preventDefault();
    setSearch(e.target.value);
    await API.get("http://localhost:8000/posts?key=" + e.target.value || "")
      .then(({ data }) => {
        setRows(data);
        navigate("/dashboard");
      })
      .catch((e) => {
        console.log(e.response.data.message);
      });
  };

  return (
    <div>
      <Layout>
        <Grid
          container
          justifyContent="flex-end"
          style={{ margin: "25px 25px 0px -25px" }}
        >
          <Button
            style={{
              backgroundColor: "#3C4043",
              color: "white",
              boxShadow:
                "0 5px 11px 0 rgb(0 0 0 / 18%), 0 4px 15px 0 rgb(0 0 0 / 15%)",
            }}
            variant="outlined"
            onClick={handleClickOpen}
          >
            Add
          </Button>
          <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogTitle>Add Product</DialogTitle>
            <DialogContent>
              <form className={classes.root} onSubmit={handleSubmit}>
                <FormControl>
                  <TextField
                    id="outlined-basic"
                    label="name"
                    variant="outlined"
                    name="name"
                    value={formData.name}
                    onChange={(e) => {
                      setFormData({
                        name: e.target.value,
                        description: formData.description,
                        price: formData.price,
                        category: formData.category,
                        image: formData.image,
                      });
                    }}
                  />
                </FormControl>
                <FormControl>
                  <TextField
                    id="outlined-basic"
                    label="description"
                    variant="outlined"
                    name="description"
                    value={formData.description}
                    onChange={(e) => {
                      setFormData({
                        name: formData.name,
                        description: e.target.value,
                        price: formData.price,
                        category: formData.category,
                        image: formData.image,
                      });
                    }}
                  />
                </FormControl>
                <FormControl>
                  <TextField
                    id="outlined-basic"
                    label="price"
                    variant="outlined"
                    name="price"
                    value={formData.price}
                    onChange={(e) => {
                      setFormData({
                        name: formData.name,
                        description: formData.description,
                        price: e.target.value,
                        category: formData.category,
                        image: formData.image,
                      });
                    }}
                  />
                </FormControl>
                <FormControl>
                  <TextField
                    id="outlined-basic"
                    label="category"
                    variant="outlined"
                    name="category"
                    value={formData.category}
                    onChange={(e) => {
                      setFormData({
                        name: formData.name,
                        description: formData.description,
                        price: formData.price,
                        category: e.target.value,
                        image: formData.image,
                      });
                    }}
                  />
                </FormControl>
                <FormControl>
                  <label htmlFor="icon-button-file">
                    Image :
                    <Input
                      accept="image/*"
                      id="icon-button-file"
                      type="file"
                      label="image"
                      name="image"
                      style={{ display: "none" }}
                      onChange={(e) => {
                        console.log(e.target.files[0]);
                        setFormData({
                          name: formData.name,
                          description: formData.description,
                          price: formData.price,
                          category: formData.category,
                          image: e.target.files[0],
                        });
                      }}
                    />
                    <IconButton
                      color="primary"
                      aria-label="upload picture"
                      component="span"
                    >
                      <PhotoCamera />
                    </IconButton>
                  </label>
                </FormControl>
                <Button
                  type="submit"
                  variant="contained"
                  style={{
                    backgroundColor: "#3C4043",
                    color: "white",
                    boxShadow:
                      "0 5px 11px 0 rgb(0 0 0 / 18%), 0 4px 15px 0 rgb(0 0 0 / 15%)",
                  }}
                >
                  Add Product
                </Button>
              </form>
            </DialogContent>
          </Dialog>
        </Grid>
        <Grid
          container
          style={{
            margin: "0px 25px 0px 25px",
            padding: "15px 20px",
            lineHeight: "0px",
          }}
        >
          <span
            style={{
              fontSize: "35px",
              fontWeight: "bold",
              fontFamily: "sans-serif",
            }}
          >
            Product
          </span>
        </Grid>
        <TextField
          style={{
            margin: "0px 25px 0px 25px",
            padding: "25px 20px 10px 20px",
            width: "80%",
          }}
          id="outlined-basic"
          variant="outlined"
          name="search"
          placeholder="Search.... "
          value={search}
          onChange={(e) => handleSearch(e)}
        />
        <Grid container style={{ padding: "0px 20px 20px 20px" }}>
          {rows?.map((row, index) => {
            return (
              <Grid
                item
                xs={12}
                sm={6}
                md={4}
                lg={3}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  marginTop: "30px",
                }}
              >
                <Card
                  style={{
                    width: "280px",
                    boxShadow:
                      "0 5px 11px 0 rgb(0 0 0 / 18%), 0 4px 15px 0 rgb(0 0 0 / 15%)",
                    borderRadius: "10px",
                  }}
                >
                  <CardMedia
                    component="img"
                    height="140"
                    image={`http://localhost:8000/` + row.image}
                    alt="green iguana"
                  />
                  <CardContent>
                    <TableContainer>
                      <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableBody>
                          <TableRow key={1}>
                            <TableCell align="left">Name:</TableCell>
                            <TableCell align="left">{row.name}</TableCell>
                          </TableRow>
                          <TableRow key={2}>
                            <TableCell align="left">Desc:</TableCell>
                            <TableCell align="left">
                              {row.description}
                            </TableCell>
                          </TableRow>
                          <TableRow key={3}>
                            <TableCell align="left">Price:</TableCell>
                            <TableCell align="left">{row.price}</TableCell>
                          </TableRow>
                          <TableRow key={4}>
                            <TableCell align="left">Category:</TableCell>
                            <TableCell align="left">{row.category}</TableCell>
                          </TableRow>
                        </TableBody>
                      </Table>
                    </TableContainer>
                  </CardContent>
                  <CardActions>
                    <>
                      <IconButton
                        onClick={() => handleClickOpenEdit(index + 1, row)}
                      >
                        <Edit />
                        &nbsp;
                        <span style={{ fontSize: "15px" }}>Edit</span>
                      </IconButton>
                      <Dialog
                        open={open1 === index + 1 ? true : false}
                        TransitionComponent={Transition}
                        keepMounted
                        onClose={handleCloseEdit}
                        aria-describedby="alert-dialog-slide-description"
                      >
                        <DialogTitle>Edit Product</DialogTitle>
                        <DialogContent>
                          <form
                            className={classes.root}
                            onSubmit={(e) => handleUpdate(e, row._id, index)}
                          >
                            <FormControl>
                              <TextField
                                id="name"
                                label="name"
                                variant="outlined"
                                name="name"
                                defaultValue={row.name}
                                onChange={(e) => {
                                  setFormData({
                                    name: e.target.value,
                                    description: formData.description,
                                    price: formData.price,
                                    category: formData.category,
                                    image: formData.image,
                                  });
                                }}
                              />
                            </FormControl>
                            <FormControl>
                              <TextField
                                id="description"
                                label="description"
                                variant="outlined"
                                name="description"
                                defaultValue={row.description}
                                onChange={(e) =>
                                  setFormData({
                                    name: formData.name,
                                    description: e.target.value,
                                    price: formData.price,
                                    category: formData.category,
                                    image: formData.image,
                                  })
                                }
                              />
                            </FormControl>
                            <FormControl>
                              <TextField
                                id="price"
                                name="price"
                                label="price"
                                variant="outlined"
                                defaultValue={row.price}
                                onChange={(e) =>
                                  setFormData({
                                    name: formData.name,
                                    description: formData.description,
                                    price: e.target.value,
                                    category: formData.category,
                                    image: formData.image,
                                  })
                                }
                              />
                            </FormControl>
                            <FormControl>
                              <TextField
                                id="category"
                                name="category"
                                label="category"
                                variant="outlined"
                                defaultValue={row.category}
                                onChange={(e) =>
                                  setFormData({
                                    name: formData.name,
                                    description: formData.description,
                                    price: formData.price,
                                    category: e.target.value,
                                    image: formData.image,
                                  })
                                }
                              />
                            </FormControl>
                            <FormControl>
                              <label htmlFor="icon-button-file">
                                <Input
                                  accept="image/*"
                                  id="icon-button-file"
                                  type="file"
                                  label="image"
                                  name="image"
                                  style={{ display: "none" }}
                                  onChange={(e) => {
                                    console.log(
                                      URL.createObjectURL(e.target.files[0])
                                    );
                                    setFormData({
                                      name: formData.name,
                                      description: formData.description,
                                      price: formData.price,
                                      category: formData.category,
                                      image: e.target.files[0],
                                    });
                                  }}
                                />
                                <img
                                  id="image"
                                  name="image"
                                  label="image"
                                  style={{ height: "65px" }}
                                  src={`http://localhost:8000/` + row.image}
                                />
                              </label>
                            </FormControl>
                            <Button
                              type="submit"
                              variant="contained"
                              style={{
                                backgroundColor: "#3C4043",
                                color: "white",
                                boxShadow:
                                  "0 5px 11px 0 rgb(0 0 0 / 18%), 0 4px 15px 0 rgb(0 0 0 / 15%)",
                              }}
                            >
                              Update
                            </Button>
                          </form>
                        </DialogContent>
                      </Dialog>
                      <IconButton
                        onClick={(e) => handleDelete(e, row._id, index)}
                      >
                        <DeleteIcon />
                        &nbsp;
                        <span style={{ fontSize: "15px" }}>Delete</span>
                      </IconButton>
                    </>
                  </CardActions>
                </Card>
              </Grid>
            );
          })}
          {rows.length <= 0 ? (
            <Grid
              item
              xs={12}
              sm={6}
              md={4}
              lg={3}
              style={{
                display: "flex",
                justifyContent: "center",
                marginTop: "30px",
              }}
            >
              <Typography>Data Not Found</Typography>{" "}
            </Grid>
          ) : (
            ""
          )}
        </Grid>
      </Layout>
    </div>
  );
}

export default Dashboard;
