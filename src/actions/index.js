export const addDessert = (data) => {
  return {
    type: "ADD_DESSERT",
    payload: {
      id: Math.random(),
      data: data,
    },
  };
};

export const editDessert = (data, id) => {
  return {
    type: "EDIT_DESSERT",
    payload: {
      id: id,
      data: data,
    },
  };
};

export const deleteDessert = (id) => {
  return {
    type: "DELETE_DESSERT",
    id,
  };
};
