import {
  FormControl,
  TextField,
  useFormControl,
  Input,
  InputLabel,
  Button,
  Snackbar,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import Stack from "@mui/material/Stack";
import MuiAlert from "@mui/material/Alert";
import Layout from "../Layouts/index";
import API from "../api"
const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(2),
    border: "2px solid grey",
    borderRadius: "15px",
    padding: "35px",
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "300px",
    },
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
    },
  },
}));

const Login = (props) => {
  const classes = useStyles();
  let navigate = useNavigate();
  const [formData, setFormData] = useState({
    username: "",
    password: "",
    email: "",
  });

  const [open, setOpen] = React.useState(false);
  const [snackMessage, setSnackMessage] = React.useState("");

  const handleClose = () => {
    setSnackMessage("");
    setOpen(false);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (formData.username !== "" && formData.password !== "") {
      API
        .post("http://localhost:8000/register", formData)
        .then(({ data }) => {
          localStorage.setItem("authtoken", data.token);
          localStorage.setItem("user", JSON.stringify(data.user));
          navigate("/dashboard");
        })
        .catch((e) => {
          setOpen(true);
          setSnackMessage(e.response.data.message);
          console.log(e.response.data.message);
        });
    }
  };

  return (
    <Layout>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        open={open}
        autoHideDuration={3000}
        onClose={handleClose}
      >
        <Alert onClose={handleClose} severity="error" sx={{ width: "100%" }}>
          {snackMessage}
        </Alert>
      </Snackbar>

      <div
        style={{
          // height:"100%",
          justifyContent: "center",
          justifyItems: "center",
          display: "flex",
          marginTop: "100px",
        }}
      >
        <form className={classes.root} onSubmit={handleSubmit}>
          <p style={{ fontSize: "22px" }}>Register</p>
          <FormControl>
            <TextField
              id="outlined-basic"
              label="username"
              variant="outlined"
              name="username"
              value={formData.username}
              onChange={(e) => {
                setFormData({
                  username: e.target.value,
                  password: formData.password,
                  email: formData.email,
                });
              }}
            />
          </FormControl>
          <FormControl>
            <TextField
              id="outlined-basic"
              label="password"
              variant="outlined"
              name="password"
              type="password"
              value={formData.password}
              onChange={(e) =>
                setFormData({
                  username: formData.username,
                  password: e.target.value,
                  email: formData.email,
                })
              }
            />
          </FormControl>
          <FormControl>
            <TextField
              id="outlined-basic"
              label="email"
              variant="outlined"
              name="email"
              type="email"
              value={formData.email}
              onChange={(e) =>
                setFormData({
                  username: formData.username,
                  password: formData.password,
                  email: e.target.value,
                })
              }
            />
          </FormControl>
          <Button type="submit" variant="contained">
            Register
          </Button>
          <Link to="/" style={{ textDecoration: 'none' }}>
          Login now!
          </Link>
        </form>
      </div>
    </Layout>
  );
};

export default Login;
